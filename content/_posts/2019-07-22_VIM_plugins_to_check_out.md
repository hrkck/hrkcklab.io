---
title: VIM plugins to check out
tags: vim,plugins
url: VIM_plugins_to_check_out
baseUrl: 
date: 2019-07-22
---

- [VIM-fugitive](https://github.com/tpope/vim-fugitive)
- [surround](https://vimawesome.com/plugin/surround-vim)
- an interesting small plugin for keeping logs: [vim-logbook](https://github.com/jamesroutley/vim-logbook)
- [vim-multiple-cursors](https://github.com/terryma/vim-multiple-cursors)
- [auto-pairs](https://github.com/jiangmiao/auto-pairs)
- [CTRLP.vim](https://github.com/ctrlpvim/ctrlp.vim)
- [vim-easy-align](https://github.com/junegunn/vim-easy-align)
