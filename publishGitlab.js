var ghpages = require('gh-pages');

console.log("PUBLISHING TO GITLAB:")

ghpages.publish('index.html', {
  dest: 'public',
  branch: 'master',
    repo: 'git@gitlab.com:hrkck/hrkck.gitlab.io.git',
}, ()=>{console.log("PUBLISHED: index.html")});

ghpages.publish('bin/app.js', {
  dest: 'public/bin',
  branch: 'master',
  repo: 'git@gitlab.com:hrkck/hrkck.gitlab.io.git',
}, ()=>{console.log("PUBLISHED: bin/app.js")});

console.log("PUBLISHED TO GITLAB.")

// module.exports.deployMe = ghpages.publish('.', {
//   branch: 'master',
//   repo: 'https://gitlab.com/01k/01kuc.gitlab.io'
// }, ()=>{});
